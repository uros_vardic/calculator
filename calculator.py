from enum import Enum

INTEGER, PLUS, MINUS, MUL, DIV, EOF, LPAREN, RPAREN, EQUAL, UNEQUAL, GREATER, LESSER, GEQUAL, LEQUAL, VARIABLE, AS = (
    'INTEGER', 'PLUS', 'MINUS', 'MUL', 'DIV', 'EOF', 'LPAREN', 'RPARN', 'EQUAL', 'UNEQUAL', 'GREATER', 'LESSER',
    'GEQUAL', 'LEQUAL', 'VARIABLE', 'AS'
)

variables = {}


class State(Enum):
    INFIX = 0
    POSTFIX = 1
    PREFIX = 2


class Token:

    def __init__(self, token_type, value):
        self.token_type = token_type
        self.value = value

    def __repr__(self):
        return "<{}, {}>".format(self.token_type, self.value)


class Lexer:

    def __init__(self, text, state):
        self.position = 0
        self.text = text
        self.state = state
        self.current_char = self.text[self.position]
        self.numbers = []
        self.operands = []
        self.roman_numbers = {'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
        self.format()

    def format(self):
        if self.state == State.INFIX:
            return

        self.split()

        formatted_text = []
        operand_counter = 0

        for number in self.numbers:
            formatted_text.append(number)

            if operand_counter < len(self.operands):
                formatted_text.append(self.operands[operand_counter])
                operand_counter += 1

        self.text = ''

        for ft in formatted_text:
            self.text += str(ft)

        self.current_char = self.text[self.position]
        # print(self.text)

    def split(self):
        while self.current_char is not None:
            if self.current_char.isspace():
                self.skip_whitespace()
                continue

            if self.current_char.isdigit():
                number = self.integer().value
                self.numbers.append(number)

            elif self.current_char == '!':
                self.advance()

                if self.current_char == '=':
                    self.advance()
                    self.operands.append('!=')
                else:
                    self.error()

            elif self.current_char == '<':
                self.advance()

                if self.current_char == '=':
                    self.advance()
                    self.operands.append('<=')
                else:
                    self.operands.append('<')

            elif self.current_char == '>':
                self.advance()

                if self.current_char == '=':
                    self.advance()
                    self.operands.append('>=')
                else:
                    self.operands.append('>')

            elif self.current_char == 'R':
                self.advance()
                self.numbers.append(self.roman().value)

            elif self.current_char.isalpha():
                self.numbers.append(self.current_char)
                self.advance()

            else:
                self.operands.append(self.current_char)
                self.advance()

        self.position = 0
        self.current_char = self.text[self.position]

    def get_next_token(self):
        while self.current_char is not None:
            if self.current_char.isspace():
                self.skip_whitespace()
                continue

            if self.current_char.isdigit():
                return self.integer()

            if self.current_char == '+':
                self.advance()
                return Token(PLUS, '+')

            if self.current_char == '-':
                self.advance()
                return Token(MINUS, '-')

            if self.current_char == '*':
                self.advance()
                return Token(MUL, '*')

            if self.current_char == '/':
                self.advance()
                return Token(DIV, '/')

            if self.current_char == '(':
                self.advance()
                return Token(LPAREN, '(')

            if self.current_char == ')':
                self.advance()
                return Token(RPAREN, ')')

            if self.current_char == ':':
                self.advance()
                return Token(EQUAL, ':')

            if self.current_char == '!':
                self.advance()

                if self.current_char == '=':
                    self.advance()
                    return Token(UNEQUAL, '!=')
                else:
                    self.error()

            if self.current_char == '<':
                self.advance()

                if self.current_char == '=':
                    self.advance()
                    return Token(LEQUAL, '<=')
                else:
                    return Token(LESSER, '<')

            if self.current_char == '>':
                self.advance()

                if self.current_char == '=':
                    self.advance()
                    return Token(GEQUAL, '>=')
                else:
                    return Token(GREATER, '>')

            if self.current_char == 'R':
                name = 'R'
                self.advance()
                for i in range(3):
                    if self.current_char is None:
                        break
                    # if self.current_char is None:
                    #     self.position -= i + 1
                    #     self.current_char = self.text[self.position]
                    #     return self.variable()
                    name += self.current_char
                    self.advance()
                if name == 'RIM(':
                    return self.roman()
                else:
                    self.position -= 4
                    self.current_char = self.text[self.position]
                    return self.variable()

            if self.current_char.isalpha():
                return self.variable()

            else:
                self.error()

        return Token(EOF, None)

    def skip_whitespace(self):
        while self.current_char is not None and self.current_char.isspace():
            self.advance()

    def integer(self):
        number = ''

        while self.current_char is not None and self.current_char.isdigit():
            number += self.current_char
            self.advance()

        return Token(INTEGER, int(number))

    def variable(self):
        var = self.current_char
        self.advance()

        while self.current_char is not None and not self.current_char.isspace():
            var += self.current_char
            self.advance()

        if self.current_char is None and var not in variables:
            return Token(VARIABLE, self.current_char)

        if self.current_char is not None and self.current_char.isspace():
            self.skip_whitespace()

        if self.current_char == '=':
            self.advance()

            if self.current_char.isspace():
                self.skip_whitespace()

            if self.current_char == 'R':
                self.advance()
                roman_number = self.roman().value

                if roman_number is not None:
                    variables[var] = roman_number

                return Token(INTEGER, roman_number)

            if self.current_char == '(':
                expr = ''
                self.advance()

                while self.current_char != ')':
                    expr += self.current_char
                    self.advance()

                lex = Lexer(expr, State.INFIX)
                inter_tmp = Interpreter(lex)
                res = inter_tmp.expr()

                variables[var] = res
                self.advance()
                return Token(INTEGER, res)
            elif self.current_char.isdigit():
                value = self.integer().value
                variables[var] = value
                self.advance()
                return Token(INTEGER, value)
            else:
                self.error()

        if var in variables:
            number = variables[var]
            return Token(INTEGER, number)

        return Token(VARIABLE, self.current_char)

    def advance(self):
        self.position += 1

        if self.position > len(self.text) - 1:
            self.current_char = None
        else:
            self.current_char = self.text[self.position]

    def error(self):
        raise Exception('Unexpected char {}'.format(self.current_char))

    def roman(self):
        # name = 'R'
        # for i in range(3):
        #     name += self.current_char
        #     self.advance()
        #
        # if name != 'RIM(':
        #     raise Exception('Unexpected function name: {}'.format(name))

        number = ''

        while self.current_char != ')':
            if self.current_char is None:
                self.error()

            number += self.current_char
            self.advance()

        self.advance()

        return Token(INTEGER, self.roman_to_integer(number))

    def roman_to_integer(self, roman_number):
        if not isinstance(roman_number, type("")):
            raise TypeError('Expected string, got {}'.format(type(roman_number)))

        roman_number = roman_number.upper()
        total = 0

        for i in range(len(roman_number)):
            try:
                value = self.roman_numbers[roman_number[i]]

                # If the next place holds a larger number
                if i + 1 < len(roman_number) and self.roman_numbers[roman_number[i + 1]] > value:
                    total -= value
                else:
                    total += value
            except KeyError:
                raise ValueError('Input is not a valid Roman number: {}'.format(roman_number))

        return total


class Interpreter:

    def __init__(self, lexer):
        self.lexer = lexer
        self.current_token = self.lexer.get_next_token()

    def expr(self):
        result = self.term()

        while self.current_token.token_type in (PLUS, MINUS):
            token = self.current_token

            if token.token_type == PLUS:
                self.eat(PLUS)
                result = result + self.term()

            elif token.token_type == MINUS:
                self.eat(MINUS)
                result = result - self.term()

        return result

    def term(self):
        result = self.factor()

        while self.current_token.token_type in (MUL, DIV):
            token = self.current_token

            if token.token_type == MUL:
                self.eat(MUL)
                result = result * self.factor()

            elif token.token_type == DIV:
                self.eat(DIV)
                result = result / self.factor()

            else:
                self.error()

        return result

    def factor(self):
        token = self.current_token

        if token in (EQUAL, UNEQUAL, GREATER, LESSER, GEQUAL, LEQUAL):
            self.eat(token.token_type)
            return None

        if token.token_type == INTEGER:
            self.eat(INTEGER)
            return token.value

        if token.token_type == LPAREN:
            self.eat(LPAREN)
            result = self.expr()
            self.eat(RPAREN)
            return result

    def eat(self, token_type):
        if self.current_token.token_type == token_type:
            self.current_token = self.lexer.get_next_token()
        else:
            self.error()

    def error(self):
        raise Exception('Error while parsing')


def logic_operand(text, state, operand):
    formated_text = Lexer(text, state).text
    new_position = formated_text.find(operand)

    lexer = Lexer(formated_text[0:new_position], state)
    interpreter = Interpreter(lexer)
    left = interpreter.expr()

    if '=' in formated_text:
        new_position += 1

    lexer = Lexer(formated_text[new_position + 1:], state)
    interpreter = Interpreter(lexer)
    right = interpreter.expr()

    if operand == ':':
        return left == right

    if operand == '<=':
        return left <= right

    if operand == '<':
        return left < right

    if operand == '>=':
        return left >= right

    if operand == '>':
        return left > right

    if operand == '!=':
        return left != right


if __name__ == '__main__':
    program_state = State.INFIX

    while True:
        try:
            t = input(program_state.name + ' --> ')
        except EOFError:
            break

        if not t:
            continue

        if t == 'exit':
            break

        if t == 'INFIX' or t == 'infix':
            program_state = State.INFIX
            continue

        if t == 'POSTFIX' or t == 'postfix':
            program_state = State.POSTFIX
            continue

        if t == 'PREFIX' or t == 'prefix':
            program_state = State.PREFIX
            continue

        if '<' not in t and '<=' not in t and '>' not in t and '>=' not in t and ':' not in t and '!=' not in t:
            lx = Lexer(t, program_state)
            inter = Interpreter(lx)
            r = inter.expr()
            print(r)
        else:
            pos = t.find('<=')
            if pos != -1:
                print(logic_operand(t, program_state, '<='))
                continue

            pos = t.find('<')
            if pos != -1:
                print(logic_operand(t, program_state, '<'))
                continue

            pos = t.find('>=')
            if pos != -1:
                print(logic_operand(t, program_state, '>='))
                continue

            pos = t.find('>')
            if pos != -1:
                print(logic_operand(t, program_state, '>'))
                continue

            pos = t.find(':')
            if pos != -1:
                print(logic_operand(t, program_state, ':'))
                continue

            pos = t.find('!=')
            if pos != -1:
                print(logic_operand(t, program_state, '!='))
